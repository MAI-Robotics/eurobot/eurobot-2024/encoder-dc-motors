# Encoder + DC Motors using a raspberry pi
This repo is about a system that uses Encoders and DC Motors to drive using a raspberry pi to calculate and communicate between two Arduinos. 

## Changelog
### Until Week 23
- Created the Encoder code
- Started with PID tuning
- Started with main raspi code
- 
### Until Gießen
- Wrote some tactics and worked on new logic with PID tuning
